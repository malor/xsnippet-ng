#!/usr/bin/env fab

import xsnippet.api
import xsnippet.api.db


app = xsnippet.api.make_app()


def runserver():
    from werkzeug import SharedDataMiddleware, DispatcherMiddleware
    from werkzeug.serving import run_simple

    db_upgrade('head')

    run_simple(
        "localhost", 5000, app,
        use_reloader=True, use_debugger=True, use_evalex=True,
        threaded=True
    )


def db_upgrade(revision='head'):
    with app.app_context():
        xsnippet.api.db.upgrade(revision)


def db_downgrade(revision='base'):
    with app.app_context():
        xsnippet.api.db.downgrade(revision)


def db_create_all():
    with app.app_context():
        xsnippet.api.db.create_all()


def db_drop_all():
    with app.app_context():
        xsnippet.api.db.drop_all()


def db_revision(message=None, autogenerate=True):
    with app.app_context():
        xsnippet.api.db.revision(message, autogenerate)
