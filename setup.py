import setuptools


setuptools.setup(
    name="xsnippet",
    version="0.0.1",
    url="https://github.com/xsnippet/xsnippet",
    license="BSD",
    author="xsnippet team",
    author_email="xsnippet@gmail.com",
    description="Simple pastebin service",

    packages=['xsnippet'],
    include_package_data=True,
    platforms='any'
)
