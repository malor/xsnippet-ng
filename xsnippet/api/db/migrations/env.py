import logging.config

import alembic
import sqlalchemy as sa
from flask import current_app as app

import xsnippet.api
import xsnippet.api.db.models


config = alembic.context.config
logging.config.fileConfig(config.config_file_name)

target_metadata = xsnippet.api.db.models.Base.metadata


def run_migrations_offline():
    alembic.context.configure(url=app.engine.url)

    with alembic.context.begin_transaction():
        alembic.context.run_migrations()


def run_migrations_online():
    with app.engine.connect() as connection:
        alembic.context.configure(connection=connection,
                                  target_metadata=target_metadata)

        with alembic.context.begin_transaction():
            alembic.context.run_migrations()


if alembic.context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
