import unittest
import pprint

from alembic.migration import MigrationContext
from alembic.autogenerate import compare_metadata

import xsnippet.api
import xsnippet.api.db


class TestModelsMigrationsSync(unittest.TestCase):

    def setUp(self):
        super(TestModelsMigrationsSync, self).setUp()

        class Config(object):
            DATABASE_URI = 'postgresql://malor:devel@192.168.2.2/devel'


        self.app = xsnippet.api.make_app(Config)
        self.ctx = self.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()

        super(TestModelsMigrationsSync, self).tearDown()

    def test_sync(self):
        xsnippet.api.db.upgrade()

        mc = MigrationContext.configure(self.app.engine.connect())
        diff = compare_metadata(mc, xsnippet.api.db.models.metadata)
        self.assertFalse(diff, pprint.pformat(diff, indent=2, width=20))
