import sqlalchemy as sa
import sqlalchemy.orm as orm
import sqlalchemy.ext.declarative as declarative

from xsnippet.api.db import utils


metadata = sa.MetaData(naming_convention=utils.naming_convention)
Base = declarative.declarative_base(metadata=metadata)


class Model(Base):
    __abstract__ = True

    @declarative.declared_attr
    def __tablename__(cls):
        return cls.__name__.lower() + 's'

    id = sa.Column('id', sa.Integer, primary_key=True)


snippets_tags = sa.Table(
    'snippets_tags', Base.metadata,
    sa.Column('snippet_id',
              sa.Integer,
              sa.ForeignKey('snippets.id', ondelete='CASCADE'),
              nullable=False),
    sa.Column('tag_id',
              sa.Integer,
              sa.ForeignKey('tags.id', ondelete='CASCADE'),
              nullable=False),
    sa.PrimaryKeyConstraint('snippet_id', 'tag_id')
)


class Author(Model):
    name = sa.Column('name', sa.String(128), index=True, nullable=False)


class Tag(Model):
    name = sa.Column('name', sa.String(32), index=True, nullable=False)


class Snippet(Model, utils.TimestampMixin):
    _name = sa.Column('name', sa.String(128))
    content = sa.Column('content', sa.Text, nullable=False)
    author_id = sa.Column('author_id', sa.Integer,
                          sa.ForeignKey('authors.id'))

    __table_args__ = (
        sa.Index('author_id_idx',
                 'author_id',
                 postgresql_where=author_id.isnot(None)),
        sa.Index('created_at_idx',
                 'created_at'),
    )

    _author = orm.relationship(
        Author,
        backref=orm.backref('snippets', lazy='subquery'),
        lazy='subquery'
    )
    _tags = orm.relationship(Tag, lazy='subquery', secondary=snippets_tags)

    @property
    def author(self):
        return self._author.name if self._author else 'Anonymous'

    @property
    def name(self):
        return self._name or 'Untitled'

    @property
    def tags(self):
        return [tag.name for tag in self.tags]
