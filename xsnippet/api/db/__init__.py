import os

import alembic
from alembic.config import Config
import sqlalchemy as sa
import sqlalchemy.orm as orm
from flask import current_app

from xsnippet.api.db.models import metadata
from xsnippet.api.db.utils import Query


alembic_cfg = Config(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), 'alembic.ini'),
)


__all__ = (
    'init_db',
    'create_all',
    'drop_all',
    'upgrade',
    'downgrade',
    'stamp',
    'revision',
)


def init_db(app):
    engine = sa.create_engine(app.config['DATABASE_URI'])
    session = orm.scoping.scoped_session(orm.sessionmaker(bind=engine,
                                                          query_cls=Query))

    def commit_session(response_or_exc):
        session.commit()
        session.remove()

        return response_or_exc
    app.teardown_appcontext(commit_session)

    app.engine = engine
    app.session = session
    return app


def create_all():
    metadata.create_all(current_app.engine)


def drop_all():
    metadata.drop_all(current_app.engine)


def upgrade(revision='head', sql_only=False):
    alembic.command.upgrade(alembic_cfg, revision, sql=sql_only)


def downgrade(revision='base', sql_only=False):
    alembic.command.downgrade(alembic_cfg, revision, sql=sql_only)


def stamp(revision):
    alembic.command.stamp(alembic_cfg, revision)


def revision(message=None, autogenerate=True):
    alembic.command.revision(alembic_cfg, message, autogenerate)
