import datetime

import flask
import sqlalchemy as sa
import sqlalchemy.orm as orm


naming_convention = {
    'columns': lambda cons, tbl: '_'.join(c.name for c in cons.columns),
    'ix': 'ix_%(table_name)s_%(columns)s',
    'uq': 'uq_%(table_name)s_%(columns)s',
    'ck': 'ck_%(table_name)s_%(constraint_name)s',
    'fk': 'fk_%(table_name)s_%(columns)s_%(referred_table_name)s',
    'pk': 'pk_%(table_name)s'
}


class TimestampMixin(object):
    created_at = sa.Column('created_at', sa.DateTime,
                           default=datetime.datetime.utcnow())
    updated_at = sa.Column('updated_at', sa.DateTime,
                           onupdate=datetime.datetime.utcnow())


class Query(orm.Query):
    def get_or_404(self, ident):
        result = self.get(ident)
        if result is None:
            flask.abort(404)

        return result
