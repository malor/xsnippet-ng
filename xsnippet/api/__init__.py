import flask

import xsnippet.api.db


__all__ = (
    'make_app',
)


def make_app(conf=None):
    app = flask.Flask(__name__)

    app.config.from_object('xsnippet.api.config.Config')
    app.config.from_envvar('XSNIPPET_API_CONFIG', silent=True)
    if conf:
        app.config.from_object(conf)

    app = xsnippet.api.db.init_db(app)

    return app
